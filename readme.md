**L'association a amélioré ses soutils de déploiement. Elle utilise maintenant une clé Ventoy permettant le démarrage aussi bien sur des systèmes UEFI que Legacy.
Toutes les explications sont données ici : https://framagit.org/philippe-dpt35/reso-outils**

Cependant la solution présentée ici reste utilisée sur certaines machines sur lesquelles ventoy ne fonctionne pas correctement.

**Sommaire** :

1. Utilité
2. Construction de l'outil

   1. Préparation du support
   2. Construction du système 64 bits

      1. Installation du système
      2. Opérations à faire dans le système
      3. Les outils
   3. Construction du système 32 bits

      1. Installation du système
      2. Opérations à faire dans le système
   4. Construction du système Clonezilla personnalisé

      1. Personnalisation de Clonezilla
      2. Installation du système sur la partition
      3. Les scripts
   5. Création du menu de démarrage
3. Utilisation de l'image prête à l'emploi

## 1. Utilité

Multi-systèmes personnalisé permettant de sauvegarder, cloner et restaurer facilement disques et partitions dans le cadre, notamment, du déploiement rapide de systèmes sur des PC.

On peut ainsi procéder à l'installation en une dizaine de minutes d'un système dont on aura réalisé une image. Les opérations sont faites avec Clonezilla en tâche de fond.

## 2. Construction de l'outil

### 2.1 Préparation du support

Sur une clé USB, un disque dur externe ou une machine virtuelle (une VM permettra de maintenir à jour les systèmes, d'y ajouter des fonctionnalités, et de construire et mettre à jour facilement des clés USB), on crée 4 partitions :

- une partition ext4 de 7,5 GO destiné à accueillir un système
  graphique en 64 bits, avec l'étiquette "DEPLOIEMENT-64", ou toute
  autre étiquette de son choix ;
- une partition ext4 de 5 Go destinée à accueillir un système
  graphique en 32 bits, avec   l'étiquette "DEPLOIEMENT-32", ou toute
  autre étiquette de son choix ;
- une partition ext4 ou FAT32 de 2 Go destinée à accueillir un système
  live personnalisé de Clonezilla en mode texte, avec l'étiquette
  "CLONEZILLA", ou toute autre étiquette de son choix ;
- une partition NTFS du reste de l'espace disponible, destinée à
  recueillir les images systèmes que l'on sauvegardera ou restaurera,
  avec l'étiquette "IMAGES-SYSTEMES" qui, elle, est    impérative pour
  le bon fonctionnement du script "Deploiement RESO". C'est sur cette
  partition que les images créées avec le script deploiement-reso sont
  enregistrées, ou lues pourrestauration.

Les systèmes des deuxième et troisième partitions serviront sur des machines ne pouvant démarrer sur un système 64 bits ou sur un système graphique.

### 2.2 Construction du système 64 bits

#### 2.2.1 Installation du système

On installe une Debian amd64 (64 bits) sur la première partition de 8 Go avec une iso netinstall.

Lors de l'installation en netinstall, on demande un montage manuel des partitions pour définir la première partition de 7,5 Go comme point de montage racine /, et étiquetée DEPLOIEMENT-64. On ne crée pas de partition de swap. On créera par la suite un fichier swap si on le juge nécessaire. Mais ça n'est pas indispensable.

Lorsque c'est demandé, on paramètre un utilisateur que l'on supprimera par la suite, et un mot de passe root. On sélectionne un bureau LXDE et les utilitaires usuels du système. On décoche l'environnement de bureau Debian si celui-ci est présélectionné, ainsi que les autres éventuelles options présélectionnées sauf les utilitaires habituels du système. On installe l'outil de démarrage Grub.

#### 2.2.2 Opérations à faire dans le système

Au premier démarrage, on se connecte en root sur l'écran de connexion. Dans la session, on ouvre un terminal pour effectuer les diverses opérations suivantes :

suppression des paquets inutiles par

apt remove --purge libreoffice*
apt autoremove
Installation des outils nécessaires ou complémentaires par

apt install network-manager clonezilla nemo gnome-terminal yad neofetch
Nota: gnome-terminal est nécessaire pour ouvrir un terminal depuis le gestionnaire de fichiers Nemo.

Facultatif : installation de quelques utilitaires tels que gparted, testdisk, gnome-disk-utility, smartmontools, gsmartcontrol, hardinfo, baobab, lshw-gtk, network-manager-gnome, xarchiver...

Pour se connecter automatiquement en root, dans /etc/lightdm/lightdm.conf, décommenter et modifier les lignes suivantes de façon à avoir :

autologin-guest=false
autologin-user=root
autologin-user-timeout=0
Dans /etc/pam.d/lightdm-autologin, changer

auth required pam_succeed_if.so user != root quiet_success
en

auth required pam_succeed_if.so user != anything quiet_success
Suppression de l'utilisateur créé lors de l'installation par

userdel nom-utilisateur
On copie les scripts deploiement-reso, disk-backup, parts-backup, infosys, outil-chroot dans /usr/bin, et on les rend exécutables ainsi que lisibles par tous par

chmod 755 /usr/bin/deploiement-reso /usr/bin/disk-backup /usr/bin/parts-backup /usr/bin/infosys /usr/bin/outil-chroot
On copie les lanceurs des scripts deploiement-reso.desktop, dusk-backup.desktop, parts-backup.desktop, infosys.desktop et outil-chroot.desktop dans le dossier /usr/share/applications, et l'icône information.ico dans /usr/share/pixmaps.

On pourra les ajouter au bureau depuis le menu principal pour un accès plus rapide.

On crée le dossier utilisé par défaut par Clonezilla pour y placer les images des systèmes :

mkdir /home/partimag
On place le dossier RESO dans le répertoire /home. Ce dossier contient divers fichiers bureautique ou mutlimédias permettant de tester le bon fonctionnement d'applications et de périphériques. On y place également le paquet anydesk, de prise de contrôle de PC à distance, à télécharger ici https://anydesk.com/fr/downloads. Si nécessaire ce dossier est à placer dans le répertoire /home des PC déployés comme dossoier ressources.

On effectue un nettoyage pour réduire l'occupation disque :

apt autoremove
apt clean
**Installation de boot-repair  :**

se rendre sur https://launchpad.net/~yannubuntu/+archive/ubuntu/boot-repair/+packages, Télécharger les divers .deb de boot-repair et de glade2script. On installe gdebi puis ces divers paquets par son intermédiaire plutôt que par dpkg. Gdebi gère mieux les dépendances.

**Paramétrages :**

Remplacer le lanceur de PCmanFM de la barre des tâches par celui de Nemo.
Désactiver l'économiseur d'écran ainsi que le verrouillage d'écran dans les préférences pour ne pas revenir à l'écran de sessions.

#### 2.2.3 Les outils RÉSO

Outre les utilitaires disponibles dans les dépôts officiels installés dans le système, on dispose de quelques outils développés pour l'association RÉSO.

**Déploiement RÉSO**

Permet de sauvegarder ou restaurer un disque entier depuis ou vers le disque principal /de/sda d'un PC. Pour un disque autre que le disque principal, une option permet de travailler sur un disque externe en USB. Cet outil est particulièrement utile pour installer très rapidement, en quelques clics, un système sur une machine à partir d'un image disque réalisée aiparavant.

IMPORTANT : si l'on déploie un système sur un disque de plus grande taille, toutes les partitions seront agrandies proportionnellement. Si par exemple l'image disque a une partition de swap de 2 Go sur un disque de 100 Go, on obtiendra un partition de swap de 20 Go si onla déploie sur un disque de 1 To. Il faut donc bien veiller à la structure et à la taille du système de fichier qui servira à réaliser l'image disque.

L'association RÉSO construit des images de petite taille dans des machines virtuelles en n'utilisant pas de partition de swap et une seule partition système. Les distributions Ubuntu et dérivées n'utilisent pas nativement de partition mais un fichier pour le swap. Pour les distributions Debian qui créent une partition de swp par défaut, il faut modifier ce comportement à l'installation du système qui sera utilisé pour le déploiement.

**Disk backup**

Permet de sauvegarder/restaurer ou cloner rapidement des disques entiers en quelques clics. Les opérations peuvent se faire depuis ou vers n'importe quel périphérique ou dossier. Lors d'une restauration ou d'un clonage, es partitions ne sont pas redimensionnées.

Cet outil, avec Parts backup, constitue un utilitaire de sauvegarde/restauration similaire à ce que peuvent faire des logiciels payants tels que Acronis True image, Easus todo backup, ..., ainsi que des logiciels gratuits tels que REDO backup, Clonezilla.

Disk backup et Parts backup sont en fait une surcouche graphique à Clonezilla dont ils utilisent les outils. Ils prennent en charge tous types de tables de partitions.

Nota : avec Disk backup la restauration n'agrandit pas les partitions proportionnellement. Elles conservent leur taille d'origine.

**Parts backup**

Permet de sauvagarder/restaurer ou cloner des partitions.

**Infos systèmes**

Script reprenant les informations données par neofetch en y ajoutant les informations disques données par la commande lsblk.

**Outil chroot**

Script permettant d'effectuer facilement un change root sur une partition son choix. Les dossiers systèmes nécessaires sont automatiquemnt montés. En sortie de chroot il est proposé de conserver ou non lesmontages.

### 2.3 Construction du système 32 bits

#### 2.3.1 Installation du système

On installe une Debian i386 (32 bits)sur la seconde partition avec une iso netinstall.

Lors de l'installation en netinstall, on demande un montage manuel des partitions pour définir la seconde partition de 5 Go comme point de montage racine /, et étiquetée DEPLOIEMENT-32. On ne crée pas de partition de swap. On créera par la suite un fichier swap si on le juge nécessaire. Mais ça n'est pas indispensable.

Lorsque c'est demandé, on paramètre un utilisateur que l'on supprimera par la suite, et un mot de passe root. On sélectionne un bureau LXDE et les utilitaires usuels du système. On décoche l'environnement de bureau Debian si celui-ci est présélectionné, ainsi que les autres éventuelles options présélectionnées sauf les utilitaires habituels du système. On n'installe pas le gestionnaire de démarrage (GRUB), car celui-ci a déjà été installé avec notre premier système. Il suffira de le mettre à jour par la suite.,

Après installation on redémarre sur notre premier système et on met à jour le Grub depuis un terminal :

update-grub
#### 2.3.2 Opérations à faire dans le système

On redémarre sur notre second système. On se connecte en root sur l'écran de connexion. Dans la session, on ouvre un terminal pour effectuer les mêmes opérations que pour le système 64 bits, sauf l'installation de boot-repair qui ne fonctionne pas sur cette version 32 bits.

### 2.4 Construction du système Clonezilla personnalisé

Pour cette troisième partition, on ne fait pas d'installation complète d'un système mais construisons un système live.

Les distributions live fonctionnent depuis un système en lecture seule compressé dans un fichier filesystem.squashfs.

Sur le site de Clonezilla il est possible de télécharger un live au format iso ainsi qu'au format zip. Télécharger le zip et le décompresser dans un répertoire de travail.

#### 2.4.1- Personnalisation de Clonezilla

Pour avoir Clonezilla directement en français et avec un clavier AZERTY sans avoir à passer par les menus de configuration, il faut modifier les fichiers /syslinux/syslinux.cfg pour le boot USB et /syslinux/isolinux.cfg pour le boot sur CD.

Dans les options des entrées de menu (on peut se contenter de ne modifier que l'entrée correspondant au démarrage par défaut), après

locales=
et

keyboards_layout
on saisit

locales=fr_FR.UTF-8
keyboards_layout=fr
**Intégration des scripts clone-disque, restaure-disque, restaure-disque-usb, monte-images**

L'explication de l'utilité et du fonctionnement de ces différents scripts est donnée plus bas.

Pour que les scripts soient disponibles comme commandes dans Clonezilla, il faut modifier le filesystem.squashfs.

On le copie dans un second répertoire de travail, puis on le décompresse par

sudo unsquashfs -d */dossier/décompression* filesystem.squashfs
pour le décompresser dans le dossier /dossier/décompression qui ne doit pas exister préalablement ou

sudo unsquashfs filesystem.squashfs
qui crée un répertoire squashfs-root contenant le système décompressé.

On se rend dans ce répertoire. On insère les scripts clone-disque, restaure-disque, restaure-disque-usb, monte-images dans le sous-répertoire /usr/bin

Il faudra faire la copie par sudo ou en root car le dossier de décompression de l'image appartient à root et n'est pas modifiable par les autres.

On reconstruit le filesystem compressé en passant la commande suivante dans le répertoire dans lequel se trouve le filesystem décompressé, squashfs-root si on a choisi la commande avec un nom de répertoire par défaut

sudo mksquashfs . /chemin/fichier/filesystem.squashfs
avec /chemin/fichier correspondant au dossier dans lequel on veut obtenir le nouveau filesystem.squashfs.

On insère ce filesystem.squashfs à la place de l'original dans notre premier répertoire de travail, et on supprime le fichier filesystem.squashfs.size.

#### 2.4.2- Installation du système sur la partition

Dans la troisième partition, nommée "CLONEZILLA", de la clé USB ou disque dur que l'on a préalablement préparé, on copie tous les fichiers de notre premier répertoire de travail qui contient maintenant Clonezilla avec tous les fichiers modifiés.

On peut utiliser l'archive clonezilla-live-2.7.1-22-i686-pae-RESO.zip prête à l'emploi, située dans le dossier archives, si l'on veut s'éviter la construction du système Clonezilla personnalisé.

#### 2.4.3- Les scripts

**clone-disque** ou **clone-disk**

Permet de réaliser une image de la totalité du disque contenant le système, y compris le MBR. L'image est automatiquement créée sur la partition contenant les images système.

Usage: `clone-disque nom-image`

nom-image étant le nom que l'on souhaite donner à l'image du système.

**restaure-disque** ou **restore-disk**

Permet de restaurer une image disque depuis la partition contenant les images système automatiquement montée.

Usage : `restaure-disque nom-image`

nom-image devant correspondre à l'une des images système disponibles sur la partition "IMAGES-SYSTEMES".

Le disque sur lequel on effectue la copie doit être de taille égale ou supérieure à celle du disque d'origine.

**restaure-disque-usb** ou **restore-disk-usb**

Permet la restauration d'un disque qui n'est pas le disque dur de la machine sur laquelle on travaille. Cela permet un déploiement à partir d'une machine tiers avec la clé ou le disque contenant Clonezilla monté en USB, et le disque de la machine que l'on veut déployer également monté en USB.

Usage : `restaure-disque-usb /chemin/usb nom-image`

- /chemin/usb étant le chemin du périphérique USB sur lequel on veut
  restaurer le système, sous la forme /dev/sdx
- nom-image devant correspondre à l'une des images système disponibles
  sur la partition "IMAGES-SYSTEMES".

**monte-images** ou **mount-images**

Permet le montage automatique de la partition "IMAGES-SYSTEMES" dans Clonezilla. Ce script est utilisé par les scripts clone-disque et restaure-disque, mais peut être utilisé indépendamment.

**cree-swapfile** ou **make-swapfile**

Permet de créer automatiquement un fichier de swap de la taille demandée dans la partition indiquée.

Usage : `cree-swapfile partition taille`

- partition étant le chemin de la partition sur le périphérique, du
  type /dev/sdxx
- taille étant un nombre entier suivi d'une unité, M ou G pour Mo ou
  Go.

Exemples :

cree-swapfile /dev/sda1 1500M
cree-swapfile /dev/sda1 2G
### 2.5 Création du menu de démarrage

Pour que le menu de démarrage prenne en compte les différents systèmes installés, il suffit de le mettre à jour depuis le système avec lequel on l'a installé, en l'occurrence ici le système 64 bits installé sur la première partition. On démarre donc sur ce système, et dans un terminal on lance

update-grub
S'affichent alors les différentes entrées reconnues. On aura également probablement celles du système du PC de travail.

On personnalise ce menu pour qu'il soit plus lisible et on supprime les entrées inutiles.

On ouvre le fichier /boot/grub/grub.cfg, et on copie toute la partie entre les lignes

`### BEGIN /etc/grub.d/10_linux ###`

et

`### END /etc/grub.d/10_linux ###`

commençant par le premier menuentry.

On ouvre le fichier /etc/grub.d/40_custom, et on y colle tout ce que l'on vient de copier en fin de fichier.

On fait la même chose pour toute la partie se trouvant entre

`### BEGIN /etc/grub.d/30_os-prober ###`

et

`### END /etc/grub.d/30_os-prober ###`

Le fichier /boot/grub/grub.cfg est le fichier de configuration de Grub, celui que grub va lire pour afficher les différentes entrées de menu. Ce fichier est régénéré à chaque mis à jour de grub par grub-update. Pour conserver les personnalisations, il faudra donc le faire dans un fichier destiné à cela, le fichier /etc/grub.d/40_custom

On doit maintenant travailler le contenu de ce que l'on vient de copier. On ne conserve que 3 entrées, celles des 3 systèmes, et on supprime toutes les entrées correspondant aux démarrages en mode avancé ainsi que celles du système du PC sur lequel on travaille.

La partie qui se trouvait entre

`### BEGIN /etc/grub.d/10_linux ###`

et

`### END /etc/grub.d/10_linux ###`

correspond aux entrées du système qui démarre par défaut. On ne conserve que la première entrée, commençant pas menuentry (et se terminant par }) avant le premier submenu ou menuentry suivant.

On modifie également l'intitulé de l'entrée de menu pour la rendre plus explicite.

Un fichier 40_custom est donné en exemple dans le dépôt. Il n'est pas réutilisable sur un autre système car les UUID des partitions ne correspondront pas à d'autres installations.

Par sécurité, on fait une copie de /boot/grub/grub.cfg qu'on pourra remettre en place avec un système en live si les modifications aboutissent à un menu inutilisable.

On met à jour le fichier par un

update-grub
puis on modifie le fichier /boot/grub/grub.cfg pour supprimer toute ce qui se trouve entre

`### BEGIN /etc/grub.d/10_linux ###`
`### END /etc/grub.d/10_linux ###`
et entre

`### BEGIN /etc/grub.d/30_os-prober ###`
`### END /etc/grub.d/30_os-prober ###`
## 3 Utilisation de l'image prête à l'emploi

L'archive deploiement-RESO.img.zip du dossier Archives contient une image de l'outil prêt à l'emploi. Pour construire une clé USB avec cette image, il suffit de démarrer sur un live CD ou USB de Clonezilla et d'effectuer une restauration de l'image sur un dispositif USB en suivant les différents menus proposés.

La dernière partition est de petite taille afin de réduire la taille de l'image et permettre la le clonage sur des clés de capacités différentes. Une fois l'image restaurée, il faudra étendre la dernière partition NTFS étiquetée "IMAGES-SYSTEMES" au reste de l'espace disponible avec Gparted par exemple.
