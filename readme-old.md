# Déploiement de systèmes

  Cette méthode permet la création d'une image système complet personnalisé ou son installation en 5 à 10 min selon que l'on est en USB3 ou USB2 à l'aide de quelques courtes commandes depuis un shell Clonezilla. Il faut disposer d'un disque dur 2.5" avec un adaptateur SATA/USB, ou d'une clé USB de grande capacité..

Testé avec des systèmes installés en mode Legacy. Non testé en mode UEFI.

***ATTENTION*** : dans la cadre d'un déploiement sur un disque de taille supérieure à celle du disque de réalisation de l'image système, toutes les partitions sont agrandies proportionnellement, y compris la partition de swap s'il en existe une. Cela peut être problématique si le disque de destination est d'une taille beaucoup plus importante.

Si par exemple on part d'un disque dur de 80 Go avec une partition swap de 2 Go, et que l'on déploie sur un disque de 500 Go, on se retrouvera avec une partition de swap de plus de 12 Go !

Le problème se pose également avec les distributions ubuntu et dérivées qui, par défaut, créent une partition primaire de boot d'environ 500 Mo, puis une partition étendue au sein de laquelle est créée une partition ext4 pour l'installation du système. Le swap se fait dans un fichier au sein de la partition système. Dans le cadre de l'instalation sur un disque beaucoup plus grand, on se retrouvera également avec une partition de boot inutilement grande.

Il est donc recommandé de procéder de la façon suivante : installer le système sur une seule partition primaire et utiliser un clone de cette installation pour le développement. Après déploiement, on lance le script `cree-swapfile` qui permet de créer automatiquement un fichier de swap de la taille demandée.


**Sommaire** :

1. Principes

2. L'outil de déploiement

3. Les scripts

4. Personnalisation du live CD/USB de Clonezilla

5. Fonctionnement

6.  Recommandations



## 1- Principes :

On crée une clé USB ou un disque dur externe contenant 2 partitions, l'une avec un live Clonezilla personnalisé, l'autre avec les images des différents systèmes que l'on a construits ou que l'on veut construire.

On boote sur la partition Clonezilla contenant des scripts permettant de restaurer ou cloner le disque sda.

Clonezilla est un OS Linux live en mode texte. Il permet de cloner, sauvegarder, restaurer disques et partitions de tous types.

Il fonctionne sur le principe de menus successifs permettant de réaliser une tâche déterminée. En fin d'opération il propose une ligne de commande permettant de la renouveler directement sans être contraint de repasser par toutes les étapes. Les scripts introduits dans le live personnalisé s'appuient sur cette ligne de commande.



## 2- L'outil de déploiement :

On crée deux partitions sur une clé USB ou un disque dur externe de 2.5" (pour ne pas à avoir à l'alimenter).

Une première partition, nommée, "CLONEZILLA", d'1 Go, contient une image personnalisé de Clonezilla.

Une seconde partition, nommée "IMAGES-SYSTEMES" est montée automatiquement au lancement de Clonezilla. Elle permet la réalisation d'images.

**IMPORTANT** : cette seconde partition doit impérativement être nommée "IMAGES-SYSTEMES" afin de permettre son montage automatique dans Clonezilla.

## 3- Les scripts :

**clone-disque** ou **clone-disk**

Permet de réaliser une image de la totalité du disque contenant le système, y compris le MBR. L'image est automatiquement créée sur la partition contenant les images système.

Usage: `clone-disque nom-image`

*nom-image* étant le nom que l'on souhaite donner à l'image du système.


**restaure-disque** ou **restore-disk**

Permet de restaurer une image disque depuis la partition contenant les images système automatiquement montée.

Usage : `restaure-disque nom-image`

*nom-image* devant correspondre à l'une des images système disponibles sur la partition "IMAGES-SYSTEMES".

Le disque sur lequel on effectue la copie doit être de taille égale ou supérieure à celle du disque d'origine.


**restaure-disque-usb** ou **restore-disk-usb**

Permet la restauration d'un disque qui n'est pas le disque dur de la machine sur laquelle on travaille. Cela permet un déploiement à partir d'une machine tiers avec la clé ou le disque contenant Clonezilla monté en USB, et le disque de la machine que l'on veut déployer également monté en USB.

Usage : `restaure-disque-usb /chemin/usb nom-image`

*/chemin/usb* étant le chemin du périphérique USB sur lequel on veut restaurer le système, sous la forme /dev/sdx

*nom-image* devant correspondre à l'une des images système disponibles sur la partition "IMAGES-SYSTEMES".

**monte-images** ou **mount-images**

Permet le montage automatique de la partition "IMAGES-SYSTEMES" dans Clonezilla. Ce script est utilisé par les scripts clone-disque et restaure-disque, mais peut être utilisé indépendamment.

**cree-swapfile** ou **make-swapfile**

Permet de créer automatiquement un fichier de swap de la taille demandée dans la partition indiquée.

Usage : `cree-swapfile partition taille`

*partition* étant le chemin de la partition sur le périphérique, du type /dev/sdxx

*taille* étant un nombre entier suivi d'une unité, M ou G pour Mo ou Go.

Exemples : `cree-swapfile /dev/sda1 1500M`

`cree-swapfile /dev/sda1 2G`



## 4- Création du live CD/USB de Clonezilla :


Les distributions live fonctionnent depuis un système en lecture seule compressé dans un fichier filesystem.squashfs.

Sur le site de Clonezilla il est possible de télécharger un live au format iso ainsi qu'au format zip. Télécharger le zip et le décompresser dans un répertoire de travail.

Une image .iso personnalisée, ainsi que les fichiers de cette iso au format .zip, sont proposés au téléchargement. Si on les utilise on peut sauter l'étape "Personnalisation" et passer directement à l'étape "Création du live". Des liens symboliques y ont été créés afin de passer les commandes de scripts en français ou en anglais tel que proposé dans le paragraphe "3- Les scripts".

### Personnalisation

Pour avoir Clonezilla directement en français et avec un clavier AZERTY sans avoir à passer par les menus de configuration, il faut modifier les fichiers /syslinux/syslinux.cfg pour le boot USB et /syslinux/isolinux.cfg pour le boot sur CD.

Dans les options des entrées de menu (on peut se contenter de ne modifier que l'entrée correspondant au démarrage par défaut), après

`locales=`

et

`keyboards_layout`

on saisit

`locales=fr_FR.UTF-8`

`keyboards_layout=fr`

Pour que les scripts soient disponibles comme commandes dans Clonezilla, il faut modifier le filesystem.squashfs.

On le copie dans un second répertoire de travail, puis on le décompresse par

`sudo unsquashfs -d */dossier/décompression* filesystem.squashfs`

pour le décompresser dans le dossier */dossier/décompression* qui ne doit pas exister préalablement

ou

`sudo unsquashfs filesystem.squashfs`

qui crée un répertoire squashfs-root contenant le système décompressé.

On se rend dans ce répertoire. On insère les scripts **clone-disque**, **restaure-disque**, **restaure-disque-usb**, **monte-images** dans le sous-répertoire /usr/bin

Il faudra faire la copie par sudo ou en root car le dossier de décompression de l'image appartient à root et n'est pas modifiable par les autres.

On reconstruit le filesystem compressé en passant la commande suivante dans le répertoire dans lequel se trouve le filesystem décompressé, squashfs-root si on a choisi la commande avec un nom de répertoire par défaut

`sudo mksquashfs . /chemin/fichier/filesystem.squashfs`

avec /chemin/fichier correspondant au dossier dans lequel on veut obtenir le nouveau filesystem.squashfs.

On insère ce filesystem.squashfs à la place de l'original dans notre premier répertoire de travail, et on supprime le fichier filesystem.squashfs.size.

### Création du live

* Vous avez personnalisé vous-même Clonezilla :

Sur la première partition, nommée "CLONEZILLA", de notre clé USB ou disque dur que l'on a préalablement préparé, on copie tous les fichiers de notre premier répertoire de travail qui contient maintenant Clonezilla avec tous nos fichiers modifiés.

* Vous avez téléchargé le zip de Clonezilla personnalisé :

  On décompresse le contenu de l'archive .zip, et on copie tous les fichiers sur la première partition, nommée "CLONEZILLA", de notre clé USB ou disque dur que l'on a préalablement préparé.

Dans le répertoire qui contient les fichiers de Clonezilla (dans son répertoire de travail si on a personnalisé soi-même Clonezilla, dans le dossier dans lequel on a décompressé les fichiers si l'on a utilisé le .zip déjà personnalisé) on se déplace dans le dossier utils/linux et on lance le script

`sudo bash makeboot.sh /dev/sdxx`

où *sdxx* correspond à la partition que l'on veut rendre bootable, donc notre première partition de clé USB ou disque dur que l'on a créée sous le nom "CLONEZILLA".

Rappel : pour voir les partitions sous forme arborescente avec leur nom, on peut lancer la commande

`lsblk`

Notre outil de déploiement est maintenant prêt.



## 5- Fonctionnement :

On démarre sur la partition Clonezilla en USB et on demande à entrer directement en ligne de commande depuis le premier menu.

**ATTENTION** : la session de Clonezilla n'est pas une session root. Il faut donc lancer les commandes par sudo, ou passer en root par
`sudo su`

Pour cloner le disque sda,on lance la commande

`sudo clone-disque nom-image`

*nom-image* correspondant au nom que l'on souhaite donner à l'image système.

Pour restaurer le disque sda, on lance la commande

`sudo restaure-disque nom-image`

*nom-image*  devant correspondre à un nom d'image système existant dans la partition "IMAGES-SYSTEMES"

Pour afficher le contenu de la partition contenant les images système :

`cd /home/partimag`

`ls -l`

Si la partition n'a pas encore été montée, faire, avant de demander l'affichage du contenu :

`sudo monte-images`

Pour restaurer un disque monté en USB

`sudo restaure-disque-usb nom-image /dev/sdx`

avec *sdx* devant correspondre au périphérique sur lequel restaurer.

Si l'on souhaite afficher la liste des périphériques afin de vérifier le chemin de celui que l'on veut restaurer, faire

`lsblk`

 

## 5- Recommandations

Si l'on travaille sur de multiples systèmes que l'on a fréquemment à déployer, il est intéressant de travailler avec des machines virtuelles. On utilise alors une machine assez puissante qui servira à la réalisation des images. On y crée toutes les machines virtuelles des systèmes dont on a besoin. Cela permet d'en faire fréquemment des mises à jour dont on peut refaire rapidement une image. Pour cloner le système, il suffit de lancer la machine virtuelle avec l'iso personnalisée de Clonezilla au démarrage. En créant un filtre USB pour la clé USB ou le disque contenant la partition "IMAGES-SYSTEMES" et en connectant celui-ci au lancement de la machine virtuelle, on pourra recréer une image en quelques minutes.

**IMPORTANT** : les disques virtuels des machines virtuelles doivent être créés avec une taille fixe afin d'éviter les erreurs du système de fichiers lors d'une réduction ultérieure de la taille de la partition.

**A l'étude :**

Utilisation d'un Raspberry Pi 4 pour les déploiements. Le RPi4 disposant de ports USB3, le transfert de fichiers se fait rapidement. C'est en outre une machine de déploiement légère, peu encombrante, et facile à déplacer.

On disposerait d'une carte SD contenant un système avec Clonezilla personnalisé ainsi que les images système. On déconnecterait le disque dur de la machine sur laquelle faire le déploiement pour le brancher en USB sur le RPi. On y copierait alors le système avant de rebrancher le disque dur sur sa machine.
